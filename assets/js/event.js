const deSelect = document.getElementById('del');
deSelect.addEventListener("change", function() {
    const val = deSelect.options[deSelect.selectedIndex].value;
    if (val === 'Del') {
        alert('Your Order will be delivered to the Address you have provided');

    } else {
        alert('You have Opted to collect your order from our store, Please reffer to the contact page for our address');
    }


});

const ptySelect = document.getElementById('pyt');
ptySelect.addEventListener("change", () => {
    const val = ptySelect.options[ptySelect.selectedIndex].value;
    if (val === 'pp') {
        document.querySelector('#pay').style.display = 'block';
        alert('Paypal will convert your amount to USD, but no extra charges are made');

    } else {
        document.querySelector('#pay').style.display = 'none';

    }

});



var tot = document.getElementById('totalPay').innerText;
const total = Number(tot) / 15;
var tots = total.toFixed(2);

console.log('the tots=> ', tots);

paypal.Buttons({
    createOrder: function(data, actions) {
        // Set up the transaction
        return actions.order.create({
            purchase_units: [{
                amount: {
                    value: tots
                }
            }]
        });
    },

    style: {
        color: 'blue',
        shape: 'pill',
        label: 'pay',
        height: 40
    },

    // Finalize the transaction
    onApprove: function(data, actions) {
        return actions.order.capture().then(function(details) {
            // Show a success message to the buyer
            alert('Transaction completed by ' + details.payer.name.given_name + '!');
        });
    }
}).render('#pay');