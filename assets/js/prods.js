window.onscroll = function() { scrollFunction() };

function scrollFunction() {
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {

        document.getElementById("nav").style.boxShadow = '1px 1px 2px 2px rgba(185, 185, 185, 0.38)';
        document.getElementById("nav").style.background = 'linear-gradient(to bottom right, rgba(0, 140, 255, 0.692), rgba(183, 0, 255, 0.596))';
        document.getElementById("nav").style.borderBottom = 'solid thin rgba(100, 100, 255, 0.534)';
        document.getElementById("scrollBtn").style.display = "block";

    } else {
        document.getElementById("nav").style.boxShadow = 'none';
        document.getElementById("nav").style.borderBottom = 'none';
        document.getElementById("nav").style.background = 'rgba(255, 255, 255, 0.92)';

        document.getElementById("scrollBtn").style.display = "none";

    }
}

function scright(element) {
    console.log("this is the element", element);
    document.getElementById(element).scrollBy(250, 0);
}

function scleft(element) {
    console.log("this is the element", element);
    document.getElementById(element).scrollBy(-250, 0);
}



var images = ['bg2', 'bg3', 'bg4', 'bg5', 'bg6', 'bg7', 'bgFull'];


function bgChange() {
    var mynum = Math.floor(Math.random() * 7);
    if (mynum == 5 || mynum == 6) {
        document.getElementById('primary').style.color = 'rgb(155, 100, 100)';
    } else {
        document.getElementById('primary').style.color = 'white';
    }
    document.getElementById('bg').style.backgroundImage = "url('images/" + images[mynum] + ".jpg')";
    setTimeout(bgChange, 3000);
}
bgChange();

function closeFlash() {
    document.querySelector('.flash').style.display = 'none';
}

function topFunction() {
    window.scrollTo({ top: 0, behavior: 'smooth' });
}

function checkout() {
    document.querySelector('#checkOutForm').style.display = 'flex';
}

function checkoutValidate() {
    const pty = document.querySelector('#pyt');
    const del = document.querySelector('#del');

    var err = [];
    err[0] = document.getElementById('fName').value;
    err[1] = document.getElementById('lName').value;
    err[2] = document.getElementById('email').value;
    err[3] = document.getElementById('tel').value;
    err[4] = document.getElementById('address').value;
    err[5] = pty.options[pty.selectedIndex].value;
    err[6] = del.options[del.selectedIndex].value;
    err[7] = document.getElementById('zip').value;


    var erV = [];
    erV[0] = document.getElementById('eRRfName');
    erV[1] = document.getElementById('eRRlName');
    erV[2] = document.getElementById('eRRemail');
    erV[3] = document.getElementById('eRRtel');
    erV[4] = document.getElementById('eRRaddress');
    erV[5] = document.getElementById('eRRpty');
    erV[6] = document.getElementById('eRRdel');
    erV[7] = document.getElementById('eRRzip');



    var em = "<span>This feild is required</span>";
    // event.preventDefault();

    for (index in err) {
        console.log('loop');
        if (err[index] == '') {
            event.preventDefault();
            erV[index].innerHTML = em;
        } else if (index == 1) {
            if (err[index].length == 0) {
                erV[index].innerHTML = em;

            }
        } else {
            erV[index].innerHTML = null;

        }
    }


}