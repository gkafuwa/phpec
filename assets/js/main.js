document.getElementById('home').addEventListener('click', function(event) {
    event.preventDefault();
    var choice = confirm('Navigating to the home page will clear the cart. Proceed?');

    if (choice) {
        window.location.href = this.getAttribute('href');
    }
});


function toggleCat() {
    document.getElementById('caties').style.top = '3em';
    document.getElementById('overlay').style.display = 'flex';

}

function closeCat() {
    document.getElementById('caties').style.top = '-50em';
    document.getElementById('overlay').style.display = 'none';
}

function menuOpen() {
    document.getElementById('help').style.visibility = "hidden";
    document.getElementById('sidenav').style.right = 0;
    document.getElementById('overlay').style.display = 'flex';

}

function menuClose() {
    document.getElementById('sidenav').style.right = '-20em';
    document.getElementById('overlay').style.display = 'none';
    closeCat();
}

function cartOpen() {
    document.getElementById('help').style.visibility = "hidden";
    document.getElementById('overlay').style.display = 'flex';
    document.getElementById('cart').style.left = 0;
}

function cartClose() {
    document.getElementById('cart').style.left = '-20em';
    document.getElementById('overlay').style.display = 'none';
}

function closeBoth() {
    document.getElementById('help').style.visibility = "hidden";
    menuClose();
    cartClose();
    closeCat();
}


function help() {
    document.getElementById('overlay').style.display = 'flex';
    document.getElementById('help').style.visibility = "visible";
}