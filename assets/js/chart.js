var count = [];
var day = [];

var countID = [];
var catID = [];

var countPend = [];
var pend = [];

var counts = document.querySelectorAll('#count')
counts.forEach(e => {
    count.push(e.innerHTML);
});


var time = document.querySelectorAll('#time')
time.forEach(e => {
    day.push(e.innerHTML);
});

// pending ==========================================
var countP = document.querySelectorAll('#countPendign');
countP.forEach(e => {
    countPend.push(e.innerHTML);
});


var pendish = document.querySelectorAll('#pend')
pendish.forEach(e => {
    pend.push(e.innerHTML);
});

// catID ===============================================
var counter = document.querySelectorAll('#countID')
counter.forEach(e => {
    countID.push(e.innerHTML);
});


var catish = document.querySelectorAll('#catID')
catish.forEach(e => {
    catID.push(e.innerHTML);
});
// catID ===============================================

console.log(day);
console.log(count);
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: day,
        datasets: [{
            label: 'Orders Pay Day',
            data: count,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 255, 0.5)',
                'rgba(255, 206, 86, 0.5)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.5)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

var catIDChartish = document.getElementById('chartCart').getContext('2d');
var myChart2 = new Chart(catIDChartish, {
    type: 'pie',
    data: {
        labels: catID,
        datasets: [{
            label: 'Most Categories sold',
            data: countID,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 255, 0.5)',
                'rgba(255, 206, 86, 0.5)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.5)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

console.log("this is pend", pend);
console.log("this is conuntpend", countPend);
var pendChartCan = document.getElementById('pendChart').getContext('2d');
var myChart3 = new Chart(pendChartCan, {
    type: 'bar',
    data: {
        labels: ["Pending", "Completed"],
        datasets: [{
            label: 'Orders Pednig And Completed',
            data: countPend,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 255, 0.5)',
                'rgba(255, 206, 86, 0.5)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.5)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});