create  Database `jfragle`;
use `jfragle`;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `catID` int(11) NOT NULL,
  `catName` varchar(50) NOT NULL,
  `catImage` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`catID`, `catName`, `catImage`) VALUES
(1, 'BreakFast', 'brkfs.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `fId` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comment` text NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`fId`, `time`, `comment`, `name`, `email`, `phone`) VALUES
(5, '2019-08-10 20:17:30', 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Hic libero temporibus vel quidem harum autem exercitationem recusandae cum. Fugit incidunt nam, neque exercitationem at magni eveniet eligendi voluptates id ad.\r\nLorem, ipsum dolor sit amet consectetur adipisicing elit. Hic libero temporibus vel quidem harum autem exercitationem recusandae cum. Fugit incidunt nam, neque exercitationem at magni eveniet eligendi voluptates id ad.\r\n', 'Given Kafuwa', 'be2015-1303@pihe.ac.za', '+2780301325');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `ids` varchar(200) NOT NULL,
  `name` varchar(50) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `qty` int(11) NOT NULL,
  `rowid` varchar(100) NOT NULL,
  `subtotal` decimal(10,0) NOT NULL,
  `timeOrdered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0',
  `image` varchar(500) NOT NULL,
  `pty` varchar(20) NOT NULL,
  `id` varchar(11) NOT NULL,
  `Identity` int(11) NOT NULL,
  `catID` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`ids`, `name`, `price`, `qty`, `rowid`, `subtotal`, `timeOrdered`, `status`, `image`, `pty`, `id`, `Identity`, `catID`) VALUES
('Given_Kafuwa', 'Lady Million', '48', 1, '38b3eff8baf56627478ec76a704e9b52', '48', '2019-08-16 20:29:05', 1, 'p9.jpg', 'COD', '101', 1, ''),
('Given_Kafuwa', 'Lady Million', '48', 1, '6974ce5ac660610b44d9b9fed0ff9548', '48', '2019-08-17 13:57:17', 1, 'p11.jpg', 'pp', '103', 2, ''),
('Given_Kafuwa', 'Lady Million', '48', 1, '6974ce5ac660610b44d9b9fed0ff9548', '48', '2019-08-20 11:00:55', 1, 'p11.jpg', 'pp', '103', 3, ''),
('Given_Kafuwa', 'Jewerly', '48', 1, '854d6fae5ee42911677c739ee1734486', '48', '2019-08-20 11:00:55', 1, 'j11.jpg', 'pp', '202', 4, ''),
('Given_Kafuwa', 'Lady Million', '48', 1, 'c9e1074f5b3f9fc8ea15d152add07294', '48', '2019-08-20 11:00:55', 1, 'p12.jpg', 'pp', '104', 5, ''),
('Given_Kafuwa', 'Jewerly', '48', 1, 'e2c0be24560d78c5e599c2a9c9d0bbd2', '48', '2019-08-20 11:00:55', 1, 'j12.jpg', 'pp', '203', 6, ''),
('Given_Kafuwa', 'Lady Million', '48', 1, 'a3c65c2974270fd093ee8a9bf8ae7d0b', '48', '2019-08-20 11:00:55', 1, 'p16.jpg', 'pp', '108', 7, ''),
('Given_Kafuwa', 'Jewerly', '48', 1, '274ad4786c3abca69fa097b85867d9a4', '48', '2019-08-20 11:00:55', 1, 'j13.jpg', 'pp', '204', 8, ''),
('Given_Kafuwa', 'Lady Million', '48', 1, 'a97da629b098b75c294dffdc3e463904', '48', '2019-08-20 11:00:55', 1, 'p15.jpg', 'pp', '107', 9, ''),
('Given_Kafuwa', 'Lady Million', '48', 1, 'c9e1074f5b3f9fc8ea15d152add07294', '48', '2019-08-20 11:00:55', 1, 'p12.jpg', 'COD', '104', 10, 'women'),
('Given_Kafuwa', 'Stunning', '48', 1, 'a97da629b098b75c294dffdc3e463904', '48', '2019-08-20 11:00:55', 1, 'p15.jpg', 'COD', '107', 11, 'women'),
('Given_Kafuwa', 'Money man', '51', 2, '65b9eea6e1cc6bb9f0cd2a47751a186f', '102', '2019-08-20 11:00:55', 1, 'p13.jpg', 'COD', '105', 12, 'men'),
('Someone_Info', 'Orange Origami', '78', 1, 'c9e1074f5b3f9fc8ea15d152add07294', '78', '2019-08-20 11:04:06', 0, 'p12.jpg', 'COD', '104', 13, 'women');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `prodID` int(11) NOT NULL,
  `prodName` varchar(50) NOT NULL,
  `image` varchar(300) NOT NULL,
  `description` text NOT NULL,
  `price` float NOT NULL,
  `categoryID` varchar(20) NOT NULL,
  `isPerfume` int(11) NOT NULL,
  `isSpecial` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `isNew` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`prodID`, `prodName`, `image`, `description`, `price`, `categoryID`, `isPerfume`, `isSpecial`, `qty`, `isNew`) VALUES
(101, 'Spicy Men', 'p9.jpg', 'Made from burning char that has a spicy scent that will leave you with a fragrance for 24hrs.  ', 48.45, 'men', 1, 0, 40, '0000-00-00 00:00:00'),
(102, 'Green Hornet', 'p10.jpg', 'contains the most oil and is with the longest lasting power. It\'s followed by eau de parfum and eau de toilette', 78.49, 'men', 1, 0, 0, '0000-00-00 00:00:00'),
(103, 'Stone Cold', 'p11.jpg', 'If you like the fragrance of coffee, you like its smell. Unlike the word odor, which usually means a bad smell, fragrances are usually good smells.', 38.45, 'men', 1, 0, 40, '0000-00-00 00:00:00'),
(104, 'Orange Origami', 'p12.jpg', 'Made from origami leaves, it has an orange scent with breath taking fragrance that lasts for 48Hrs ', 78.45, 'women', 1, 0, 40, '0000-00-00 00:00:00'),
(105, 'Money man', 'p13.jpg', 'Fragrance made from herbal spices and rare flowers collected from  South Africa', 50.99, 'men', 1, 0, 40, '0000-00-00 00:00:00'),
(106, 'Back Omish', 'p14.jpg', 'Made from The rearrest black spices from India. May cause some allergens  ', 48.45, 'men', 1, 0, 40, '0000-00-00 00:00:00'),
(107, 'Stunning', 'p15.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet. ', 48.45, 'women', 1, 0, 40, '0000-00-00 00:00:00'),
(108, 'Lady Million1', 'p16.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet. ', 38.45, 'men', 1, 0, 41, '0000-00-00 00:00:00'),
(109, 'Lady Million', 'p17.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet.', 48.45, 'women', 1, 0, 40, '0000-00-00 00:00:00'),
(201, 'Jewerly', 'j10.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet.', 48.45, 'neck', 0, 1, 40, '0000-00-00 00:00:00'),
(202, 'Jewerly', 'j11.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet.', 48.45, 'wrist', 0, 0, 40, '0000-00-00 00:00:00'),
(203, 'Jewerly', 'j12.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet.', 48.45, 'rings', 0, 0, 40, '0000-00-00 00:00:00'),
(204, 'Rust Blue', 'j13.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet. ', 48.45, 'rings', 0, 1, 40, '0000-00-00 00:00:00'),
(205, 'Jewerly', 'j14.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet.', 48.45, 'rings', 0, 0, 40, '0000-00-00 00:00:00'),
(206, 'Green Dart', 'j15.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet. ', 48.45, 'rings', 0, 0, 40, '0000-00-00 00:00:00'),
(207, 'Wire Label', 'j16.jpg', 'This product is hand made from wires and has predifined labes on it', 48.45, '1', 0, 0, 40, '0000-00-00 00:00:00'),
(208, 'Jewerly', 'j17.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet.', 48.45, 'rings', 0, 0, 0, '0000-00-00 00:00:00'),
(209, 'Jewerly', 'j18.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet.', 48.45, 'rings', 0, 1, 40, '0000-00-00 00:00:00'),
(1002, 'Bath And Face', 'p2.jpg', 'Can be used when bathing and face wash ', 48.45, 'oils', 1, 0, 40, '0000-00-00 00:00:00'),
(1003, 'Blossom', 'p3.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet. ', 48.45, 'women', 1, 0, 40, '0000-00-00 00:00:00'),
(1004, 'Bath Oils', 'p4.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet. ', 48.45, 'oils', 1, 0, 40, '0000-00-00 00:00:00'),
(1005, 'Natural Oils', 'p5.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet. ', 48.45, 'oils', 1, 1, 40, '0000-00-00 00:00:00'),
(1006, 'ButterFly Lady', 'p6.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet. ', 48.45, 'women', 1, 0, 40, '0000-00-00 00:00:00'),
(1007, 'Lavender and Orchid', 'p7.jpg', 'Use these fragrances to keep help yu sleep, Apply a few drops on your pillow. made from lavender and Orchid leaves', 205.45, 'oils', 1, 1, 40, '0000-00-00 00:00:00'),
(1008, 'Lady Million', 'p8.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet.', 48.45, 'women', 1, 0, 40, '0000-00-00 00:00:00'),
(2001, 'Stone Age', 'j1.jpg', 'Made from stones collected from the sea. but has been scraped to smothness for better feel on the wrist', 48.45, 'wrist', 0, 0, 29, '0000-00-00 00:00:00'),
(2002, 'Jewerly', 'j2.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet.', 48.45, 'neck', 0, 0, 40, '0000-00-00 00:00:00'),
(2003, 'Jewerly', 'j3.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet.', 48.45, 'neck', 0, 0, 40, '0000-00-00 00:00:00'),
(2004, 'Flufy Band', 'j4.jpg', 'Made from silk. It\'s very light and soft on the skin.', 18.45, 'wrist', 0, 0, 40, '0000-00-00 00:00:00'),
(2005, 'Jewerly', 'j5.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet.', 48.45, 'wrist', 0, 0, 40, '0000-00-00 00:00:00'),
(2006, 'Jewerly', 'j6.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet.', 48.45, 'neck', 0, 0, 40, '0000-00-00 00:00:00'),
(2007, 'Jewerly', 'j7.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet.', 48.45, 'wrist', 0, 0, 40, '0000-00-00 00:00:00'),
(2008, 'Jewerly', 'j8.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet.', 48.45, 'wrist', 0, 0, 40, '0000-00-00 00:00:00'),
(2009, 'Jewerly', 'j9.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet.  ', 48.45, 'wrist', 0, 0, 40, '0000-00-00 00:00:00'),
(10010, 'Essence', 'p18.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet.  ', 48.45, 'women', 1, 0, 40, '0000-00-00 00:00:00'),
(10011, 'Lady Million', 'p19.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet.', 48.45, 'men', 1, 1, 40, '0000-00-00 00:00:00'),
(10012, 'Lady Million', 'p20.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet.', 48.45, 'men', 1, 0, 40, '0000-00-00 00:00:00'),
(10013, 'Lady Million', 'p21.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi reiciendis, necessitatibus recusandae pariatur rerum dolor ut illum. Commodi illum blanditiis unde quae veniam autem quis. Dolor cumque iure quaerat eveniet.', 48.45, 'women', 1, 0, 40, '0000-00-00 00:00:00'),
(20011, 'Hazel Bloom', 'j19.jpg', 'Get Both The Neckless and earing for one price. ', 148.45, 'neck', 0, 0, 40, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `superuser`
--

CREATE TABLE `superuser` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `superuser`
--

INSERT INTO `superuser` (`id`, `username`, `password`) VALUES
(1, 'Admin', 'Admin123');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `address` text,
  `telephone` varchar(20) DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  `delivery` varchar(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `address`, `telephone`, `zip`, `delivery`, `status`) VALUES
(4, 'Given_Kafuwa', 'be2015-1303@pihe.ac.za', '9 Concord Road Bedford view', '+2780301325', 2193, 'sc', 1),
(5, 'Someone_Info', 'be2015-1303@pihe.ac.za', '9 Concord Road Bedford view', '+2780301325', 2193, 'Del', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`catID`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`fId`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`Identity`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`prodID`);

--
-- Indexes for table `superuser`
--
ALTER TABLE `superuser`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `fId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `Identity` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `prodID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20012;

--
-- AUTO_INCREMENT for table `superuser`
--
ALTER TABLE `superuser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
