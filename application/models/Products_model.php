<?php
/* This Model is used to retrieve values from the database but specifically, the products and categories table */

class Products_model extends CI_Model
{

    //This will get all the products from the database
    public function get_products()
    {
        $this->db->select('*');
        $this->db->from('products');
        //not using the above so that I can implement pagination

        // $query = $this->db->get('products', '10', $this->uri->segment(3)); 
        $query = $this->db->get();
        //the '3' after the products should be the same as the per_page value of the config
        return $query->result();
    }
    //This will get all the details about a single product
    public function get_product_details($id)
    {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('prodID', $id);
        $query = $this->db->get();
        return $query->row();
    }
    //This will get all the details about a single product
    public function get_latest_prod()
    {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->limit(6);
        $query = $this->db->get();
        return $query->result();
    }
    //This will get all the related products to the details
    public function get_product_related($catId)
    {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('categoryID', $catId);
        $query = $this->db->get();
        return $query->result();
    }

    //this will load the categories from the categories table
    public function get_categories()
    {
        $this->db->select('*');
        $this->db->from('category');
        $query = $this->db->get();
        return $query->result();
    }

    //This will cross reference the orders table with the products table and return the one with the highest order count 
    public function get_popular()
    {
        $this->db->select('P.*, COUNT(O.id) as total');
        $this->db->from('orders AS O');
        $this->db->join('products AS P', 'O.id = P.prodID', 'INNER');
        $this->db->group_by('O.id');
        $this->db->order_by('total', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    //This will get the products according to their categories
    public function get_categories_view($id)
    {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('categoryID', $id);
        $query = $this->db->get();
        return $query->result();
    }

    //This will add a new product record
    public function add_prod($data = array())
    {
        $this->db->insert('products', $data);
        return true;
    }

    public function search_results($value)
    {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->like('prodName', $value);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_all_products()
    {
        $this->db->select('*');
        $this->db->from('products');
        $query = $this->db->get();
        return $query->result();
    }

    public function update_prod_table($id, $data = array())
    {
        $this->db->where('prodID', $id);
        $this->db->update('products', $data);
        return true;
    }

    public function delete_prod($del)
    {
        $this->db->where('prodID', $del);
        $this->db->delete('products');
        return true;
    }
    public function get_chart()
    {
        $this->db->select('count(timeOrdered) as time, timeOrdered as day');
        $this->db->distinct();
        $this->db->group_by("timeOrdered");
        $this->db->order_by("timeOrdered", "asc");
        $this->db->from('orders');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_chartCat()
    {
        $this->db->select('count(catID) as count, catID');
        $this->db->distinct();
        $this->db->group_by("catID");
        $this->db->order_by("catID", "asc");
        $this->db->from('orders');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_pending()
    {
        $this->db->select('count(status) as countPendign, status as pend');
        $this->db->distinct();
        $this->db->group_by("status");
        $this->db->order_by("status", "asc");
        $this->db->from('orders');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all()
    {
        return $this->db->count_all_results('orders');
        // $this->db->from('orders');
        // $query = $this->db->get(); 
        // return $query->result();
    }
}
