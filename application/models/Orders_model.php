<?php
class Orders_model extends CI_Model
{

    //inserting order from cart to database
    public function print_cart($data = array())
    {
        $insert = $this->db->insert('orders', $data);
        return $insert;
    }

    public function add_customer($data = array(), $name)
    {
        $this->db->where('name', $name);
        $result = $this->db->get('users');

        if ($result->num_rows() == 1) {
            $data = array(
                'status' => 0
            );
            $this->db->where('name', $name);
            $this->db->update('users', $data);
            return true;
        } else {
            $insert = $this->db->insert('users', $data);
            return $insert;
        }
    }

    //to add a little icon to the table number 
    //showing that an order has been placed
    public function updateTable($tabNo, $data = array())
    {
        $this->db->where('ids', $tabNo);
        $this->db->update('orders', $data);
        return true;
    }

    //view orders according to date
    public function view($data)
    {
        $this->db->select('*');
        $this->db->from('orders');
        $this->db->where('status', 0);
        $this->db->like('ids', $data);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_users()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('status', 0);
        $query = $this->db->get();
        return $query->result();
    }


    //this will update the status column to 1
    public function change_status($name)
    {
        $data = array(
            'status' => 1
        );

        $this->db->where('name', $name);
        $this->db->update('users', $data);

        return true;
    }

    public function alert($tNum, $data = array())
    {
        $this->db->where('tableNo', $tNum);
        $this->db->update('tables', $data);
        return true;
    }

    public function feedback($data = array())
    {
        $insert = $this->db->insert('feedback', $data);
        return $insert;
    }

    public function viewFeedback()
    {
        $this->db->select('*');
        $this->db->from('feedback');
        $this->db->order_by('time', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }
}
