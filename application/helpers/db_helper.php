<?php
    // Get categories
    // we are not in a Model or a controller so we have to instanciate
    function get_categories_h(){
        $CI = get_instance();// this is the instance
        $categories = $CI->Products_model->get_categories();
        return $categories;
    }
    
//this function will get all tghe popular products
    function get_popular_h(){
        $CI = get_instance();
        $CI->load->model('Products_model');
        $popular_products = $CI->Products_model->get_popular();
        return $popular_products;
    }

//this function will get all the products from the products table
  function products_h(){
        $CI = get_instance();
        $CI->load->model('Products_model');
        $products = $CI->Products_model->get_all_products();
        return $products;
  }

  function tables_h(){
    $CI = get_instance();
    $CI->load->model('Orders_model');
    $tableNo = $CI->Orders_model->tables();
    return $tableNo;
}