<div class="subscribe">
    <header>Categories</header>
    <div class="catSub">
        <div class="icon">
            <a href="<?php echo base_url(); ?>products/category/women">
                <img src="<?php echo base_url(); ?>images/svgs/deodorant.svg">
                <span>Women</span>
            </a>
        </div>
        <div class="icon">
            <a href="<?php echo base_url(); ?>products/category/oils">
                <img src="<?php echo base_url(); ?>images/svgs/lotion.svg">
                <span>Oils</span>
            </a>
        </div>
        <div class="icon">
            <a href="<?php echo base_url(); ?>products/category/neck">
                <img src="<?php echo base_url(); ?>images/svgs/pearl-necklace.svg">
                <span>Neck</span>
            </a>
        </div>
        <div class="icon">
            <a href="<?php echo base_url(); ?>products/category/rings">
                <img src="<?php echo base_url(); ?>images/svgs/rings.svg">
                <span>Rings</span>
            </a>
        </div>
        <div class="icon">
            <a href="<?php echo base_url(); ?>products/category/wrist">
                <img src="<?php echo base_url(); ?>images/svgs/wristwatch.svg">
                <span>Wrist</span>
            </a>
        </div>
        <div class="icon">
            <a href="<?php echo base_url(); ?>products/category/men">
                <img src="<?php echo base_url(); ?>images/svgs/cologne.svg">
                <span>Mens</span>
            </a>
        </div>

    </div>
    <!-- <header id="subForm">Subscribe</header> -->
    <!-- <form action="<?php echo base_url(); ?>users/subscribedd" onsubmit="return valSub();" method="post">
        <div class="valError" id="erNames">65161</div>
        <input type="text" id="names" placeholder="Enter Name">
        <div class="valError" id="erEmails">ujfcu</div>
        <input type="text" id="emails" placeholder="email@email.com">
        <button type="submit">Subscribe</button>
    </form> -->
    <span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Necessitatibus rem harum, nihil libero veniam similique voluptates amet animi debitis maiores.</span>
    <script src="<?php echo base_url(); ?>assets/js/validate.js"></script>
</div>