<header>Products by Category</header>
    <div class="contain">
        <!-- sub here -->
        <?php
        $this->load->view('subscribe');
        ?>
<div class="test">
<input id="tab1" type="radio" name="tabs" class="tabRad" checked>
<section id="content1">
    <!-- this is for perfumes -->
    <!-- create a for loop here -->
    <div class="products">
        <?php foreach ($products as $prod) {
            ?>
            <div class="card">
                <div class="info">
                    <div class="imagePlace">
                        <img src="<?php echo base_url(); ?>images/<?php echo $prod->image; ?>" alt="">
                        <div id="ad">
                            <?php if ($prod->isSpecial == 1) {
                                echo '<div id="sp"> <i class="material-icons right">stars</i> Special</div>';
                            } ?>
                            <?php if ($prod->isSpecial == 1) {
                                echo '<div id="new"> <i class="material-icons right">new_releases</i> New</div>';
                            } ?>
                        </div>
                        <?php if ($prod->qty == 0) {    ?>
                            <div id="outStock">
                                <div id="sp"><i class="material-icons right">not_interested</i> OUT OF ST0CK<i class="material-icons right">not_interested</i></div>
                            </div>
                        <?php  } ?>
                        <div class="text">
                            <a href="<?php echo base_url(); ?>Products/details/<?php echo $prod->prodID; ?>">
                                <h3>View Info</h3>
                            </a>
                        </div>
                    </div>
                    <span><?php echo $prod->prodName; ?> </span>
                    <hr class="pricething">
                    <span class="price">R<?php echo $prod->price; ?></span>
                    <p><?php
                        ?>. . .</p>
                    <hr>
                    <form method="post" action="<?php echo base_url(); ?>cart/add">
                        <div class="qty">
                            <div class="input-group">
                                <span>QTY:</span> <input type="number" value="1" id="qty" name="qty" class="quantity-field">
                            </div>
                        </div>
                        <input type="hidden" name="prodID" value="<?php echo $prod->prodID; ?>" />
                        <input type="hidden" name="price" value="<?php echo $prod->price; ?>" />
                        <input type="hidden" name="image" value="<?php echo $prod->image; ?>" />
                        <input type="hidden" name="prodName" value="<?php echo $prod->prodName; ?>" />
                        <button id="add" <?php if ($prod->qty == 0) {
                                                echo 'disabled';
                                            } ?>> <span>ADD TO CART</span> <i class="material-icons right">add_shopping_cart</i></button>
                    </form>
                </div>
            </div>
        <?php }
        ?>
        <!-- end for here -->
</section>
</div>