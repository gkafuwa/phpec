<?php if ($this->session->flashdata('feedback')) { ?>
    <div class="flash flash-success">
        <p class="head"> Info <span onclick="closeFlash();">&times</span> </p>

        <?php echo $this->session->flashdata('feedback');  ?>
    </div>
<?php } ?>

<header>Contact US</header>
<section id="conatcUS">
    <div class="sidies">
        <form action="<?php echo base_url(); ?>View_Orders/feedback" onsubmit="return valContact();" method="post">
            <div class="valError" id="erName"></div>
            <input type="text" id="name" placeholder="Enter Name" name="name">
            <div class="valError" id="erEmail"></div>
            <input type="text" id="email" placeholder="email@email.com" name="email">
            <input type="text" id="phone" placeholder="+27 665 546 5646" name="phone">
            <div class="valError" id="erMessage"></div>
            <textarea id="message" rows="10" placeholder="Enter message" name="message"></textarea>

            <button type="submit">submit</button>
        </form>
        <div class="info">
            <h4>Store</h4>
            <p><i class="material-icons">pin_drop</i>Our Shop</p>
            <p><i class="material-icons">local_phone</i> +27 115 325 006</p>
            <p><i class="material-icons">alternate_email</i> info@jfragle.com</p>
            <h4>We accept</h4>
            <p><i class="material-icons">payment</i></i> Credit Cards</p>
            <p><i class="material-icons">attach_money</i> Cash</p>
            <br>

        </div>
    </div>
    <div class="map">
        <iframe width="100%" height="100%" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;coord=-26.172543485518727,28.133432865142822&amp;q=9%20concord%20road+(Bugger%20and%20stuff)&amp;ie=UTF8&amp;t=&amp;z=15&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/map-my-route/">
                Plot a route map</a></iframe>
    </div>

</section>
<script src="<?php echo base_url(); ?>assets/js/validate.js"></script>