
<?php if($this->session->flashdata('registered')) { ?>
     <div class="alert alert-success">
         <?php echo $this->session->flashdata('registered');  ?>
     </div>
 <?php } ?>

<div class="panel panel-default panel-list">
    <div class="panel-heading-dark">
        <h4 class="panel-title">ADD A USER</h4>
    </div>
    <div class="panel-body">
     <?php echo validation_errors('<div class="alert alert-danger">','</div>'); ?>   
           <form method="post" action="<?php echo base_url(); ?>users/register">
            
           <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default"> User ID: </span>
                </div>
                <input type="text" name="userID" placeholder="Enter User ID" class="form-control" aria-describedby="inputGroup-sizing-default">
            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default"> User Name: </span>
                </div>
                <input type="text" name="userName" placeholder="Enter Users Name" class="form-control" aria-describedby="inputGroup-sizing-default">
            </div>


            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Access</label>
                </div>
                <select class="custom-select" id="inputGroupSelect01" name="rank">
                    <option selected>Rank...</option>
                    <option value="Manager">Manager</option>
                    <option value="Waiter">Waiter</option>
                </select>
                </div>
    
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default"> Password: </span>
                </div>
                <input type="password" name="password" placeholder="Enter password" class="form-control" aria-describedby="inputGroup-sizing-default">
            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default"> Confirm Password: </span>
                </div>
                <input type="password" name="passwordConf" placeholder="Confirm password" class="form-control" aria-describedby="inputGroup-sizing-default">
            </div>
            
            <button name="btn_submit" type="submit" class="btn btn-primary"> Register</button>
        </form>
    <br>