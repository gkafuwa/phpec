<?php if ($this->session->flashdata('fail_login')) { ?>
    <div class="flash flash-danger">
        <?php echo $this->session->flashdata('fail_login');  ?>
    </div>
<?php } ?>

<form class="login" method="post" action="<?php echo base_url(); ?>users/logins">
    <div class="cardIsh">
        <h2>Login</h2>
        <label>Username</label>
        <input type="text" id="name" name="username" placeholder="Enter Username">
        <label>Password</label>
        <input type="password" id="pass" name="pass" placeholder="Enter Password">
        <button type="submit">Login</button>

    </div>
</form>