<header> Orders
</header>

<table id="tabThing" class="tabCart">
  <tr>
    <th>Prod Name</th>
    <th>Price</th>
    <th>QTY</th>
    <th>SubTot</th>
    <th>Payment</th>
  </tr>

  <?php

  $grandTotal = 0;
  $date = "";
  foreach ($orders as $order) {

    $grandTotal +=  $order->subtotal;
    $date = $order->timeOrdered;
    $name = $order->ids;


    ?>
  <tr>
    <form action="<?php echo base_url(); ?>cart/check_out" onsubmit="return clearorder();" method="post">
      <input type="hidden" value="<?php echo $order->ids; ?>" name="ids" />

      <td><?php echo $order->name; ?></td>
      <td> R <?php echo $order->price ?></td>
      <td><?php echo $order->qty; ?></td>
      <td> R <?php echo $order->subtotal; ?></td>
      <td><?php echo $order->pty; ?></td>
  </tr>

  <?php } ?>

  <tr>
    <td>
      <b>
        <h4> Total: <?php echo 'R' . $grandTotal; ?> </h4>
      </b>
    </td>
    <td>
      <p>Order Date: <?php echo $date; ?> </p>
      <p id="nameish" hidden><?php echo $name; ?> </p>
    </td>
    <td colspan="5" align="right">
      <button type="submit" id="checkout" style="padding: 10px;" class='fg-button teal checkout'> Clear Upon Deliverly </button>
    </td>
  </tr>
  </form>
</table>
