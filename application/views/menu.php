<div id="csold">
    <h2> Products sold <br>
        <span><?php echo $all;   ?></span>
    </h2>
</div>
<div class="chart">
    <div id="menu">
        <h5>Orders made based on time</h5>
        <canvas id="myChart"></canvas>
    </div>

    <div id="menu">
        <h5>The most Categories Sold</h5>
        <canvas id="chartCart"></canvas>
    </div>

    <div id="menu">
        <h5>For orders pending against completed</h5>
        <canvas id="pendChart"></canvas>
    </div>
</div>
<!-- for the dates -->
<?php
foreach ($chart as $c) {
    echo "<div id='count' hidden>" . $c->time . "</div>";
}

foreach ($chart as $c) {
    echo "<div id='time' hidden>" . $c->day . "</div>";
}
?>

<!-- for the categories -->
<?php
foreach ($chartCat as $ct) {
    echo "<div id='countID' hidden>" . $ct->count . "</div>";
}

foreach ($chartCat as $ct) {
    echo "<div id='catID' hidden>" . $ct->catID . "</div>";
}
?>

<!-- for the oders pending -->
<?php
foreach ($pending as $ct) {
    echo "<div id='countPendign' hidden>" . $ct->countPendign . "</div>";
}

foreach ($pending as $ct) {
    echo "<div id='pend' hidden>" . $ct->pend . "</div>";
}
?>


<script src="<?php echo base_url(); ?>assets/js/chart.js"></script>