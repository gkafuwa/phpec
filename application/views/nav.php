<div class="sidenav" id="sidenav">
    <div class="closebtn" onclick="menuClose();">&times; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span>Menu</span> </div>
    <ul style="list-style-type:none;">
        <li>
            <a id="home" href="<?php echo base_url(); ?>users/logout"> <i class="material-icons right">home</i> &nbsp; &nbsp;HOME</a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>"> <i class="material-icons right">format_list_bulleted</i>&nbsp; &nbsp; PRODUCTS
            </a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>products/about"> <i class="material-icons right">info</i>&nbsp; &nbsp; ABOUT</a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>products/contactus"> <i class="material-icons right">contact_mail</i>&nbsp; &nbsp; CONTACT US</a>
        </li>
    </ul>
</div>
<nav id="nav">
    <a href="<?php echo base_url(); ?>">
        <img src="<?php echo base_url(); ?>images/headerimg.png" alt="">
        <h4>JFragle</h4>
    </a>
    <ul>
        <?php if (!empty($this->session->userdata('user_id'))) { ?>
            <li>
                <button onclick="cartOpen();">
                    <i class="material-icons right">shopping_cart</i>
                    <span class="badge alert">
                        <?php echo $rows = count($this->cart->contents()); ?>
                    </span>
                </button>
            </li>
        <?php } ?>
        <li><button onclick="help();"> <i class="material-icons right">help</i></button></li>
        <?php if (!empty($this->session->userdata('user_id'))) { ?>
            <li><button onclick="toggleCat();"> <i class="material-icons right">swap_vertical_circle</i></button></li>
        <?php } ?>
    </ul>
    <span class="closebtn" onclick="menuOpen();">&#9776; </span>
</nav>