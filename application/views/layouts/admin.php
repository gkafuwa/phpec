<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>images/headerimg.png" type="image/x-icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/test.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.js"></script>
    <title>JFragle</title>
</head>

<body>
    <nav id="nav">
        <a href="<?php echo base_url(); ?>users/menu">
            <img src="<?php echo base_url(); ?>images/headerimg.png" alt="">
            <h4>JFragle</h4>
        </a>
        <ul style="margin-right: 1em;">
            <?php if (!empty($this->session->userdata('logged_in'))) { ?>
            <a href="<?php echo base_url(); ?>view_orders">
                <li><button> 
                <i class="material-icons right">shopping_cart</i>

                </button></li>
            </a>
            <a href="<?php echo base_url(); ?>products/add">
                <li><button>
                        <i class="material-icons right">add_photo_alternate</i>
                    </button></li>
            </a>
            <a href="<?php echo base_url(); ?>products/edit">
                        
                <li><button> <i class="material-icons right">border_color</i></button></li>
            </a>
            <a href="<?php echo base_url(); ?>View_Orders/viewFeedback">
                <li><button> 
                <i class="material-icons right">mail</i>
                </button></li>
            </a>
            <a href="<?php echo base_url(); ?>users/logout">
                <li><button> 
                <i class="material-icons right">power_settings_new</i>
                </button></li>
            </a>


            <?php } ?>

        </ul>
    </nav>


    <?php $this->load->view($main_content); ?>
    <button onclick="topFunction()" id="scrollBtn" title="Go to top"><i class="material-icons right">arrow_upward</i></button>

    <footer>
        <p>&copy; 2019 JFragle
            <p>
    </footer>
    <script src="<?php echo base_url(); ?>assets/js/prods.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/validate.js"></script>
</body>

</html>