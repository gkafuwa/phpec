<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <title>Doppio Zero</title>
    <link rel="icon" href="<?php echo base_url(); ?>assets/images/images.png">
    
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/base.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
    
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
    
    <script type='text/javascript' src="<?php echo base_url(); ?>assets/jScript/bootstrap.js"></script>
    <script type='text/javascript' src="<?php echo base_url(); ?>assets/jScript/bootstrap.bundle.js.map"></script>
    <script type='text/javascript' src="<?php echo base_url(); ?>assets/jScript/bootstrap.bundle.js.map"></script>
    <script type='text/javascript' src="<?php echo base_url(); ?>assets/jScript/bootstrap.bundle.min.js"></script>
    
</head>



<body onload="window.print()">
  
<br><br> <br>
 
 
    <div class="container">

               <div class="panel panel-default">
               <div class="panel-heading panel-heading-blue">
              <?php $this->load->view($main_content); ?>  
                    