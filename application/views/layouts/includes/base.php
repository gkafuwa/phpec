<div class="cart" id="cart">
    <div class="top">
        <h2>Your Cart</h2>
        <span class="closebtn" onclick="cartClose();">&times;</span>
        <hr>
    </div>
    <div class="cartItems">
        <?php if (!empty($this->cart->contents())) { ?>
            <?php
            $cart = $this->cart->contents(); ?>
            <?php foreach ($cart as $it) {
                ?>
                <div class="item">
                    <div class="im">
                        <img src="<?php echo base_url() . 'images/' . $it['image']; ?>">
                    </div>
                    <div class="pricing">
                        <div class="prodName">
                            <?php echo $it['name']; ?>
                        </div>
                        <div class="qty">
                            <div class="input-group">
                                <span class="theQty"><span>QTY:</span></span> <input type="text" value="<?php echo $it['qty']; ?>" id="qty" class="quantity-field" disabled>
                                <a class="del" href="<?php echo base_url().'cart/delete/'.$it['rowid']?>"><i class="material-icons">remove_shopping_cart</i></a>
                            </div>
                        </div>
                        <span class="price">R <?php echo $it['subtotal']; ?></span>
                    </div>
                </div>
            <?php }
        } else {
            echo '<p>Your cart is empty</p>';
        }
        ?>
    </div>
    <div class="bottom">
        <h2>Subtotal</h2> <span>R <?php echo $this->cart->format_number($this->cart->total()); ?></span>
        <p>Taxes and shipping are calculated at Checkout</p>
        <a href="<?php echo base_url(); ?>cart">View Cart</a>
    </div>

</div>
<button onclick="topFunction()" id="scrollBtn" title="Go to top"><i class="material-icons right">arrow_upward</i></button>


<footer>
    <p>&copy; 2019 JFragle
        <p>
</footer>
<script src="<?php echo base_url(); ?>assets/js/prods.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
</body>


</html>