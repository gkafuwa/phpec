<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>images/headerimg.png" type="image/x-icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/test.css" />
    <title>JFragle</title>
</head>

<body>
    <?php $this->load->view('overlay'); ?>
    <?php $this->load->view('nav'); ?>

    <div id="caties">
        <div id="search">
            <form action="<?php echo base_url(); ?>products/search" method="POST">
                <input name="srch" type="text" placeholder="Search here...">
                <button type="submit"><i class="material-icons right">search</i></button>
            </form>
        </div>
        <?php $this->load->view('subscribe'); ?>

    </div>

