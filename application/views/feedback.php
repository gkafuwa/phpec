</div>

 <?php if($this->session->flashdata('feedback')) { ?>
     <div class="alert alert-success">
         <?php echo $this->session->flashdata('feedback');  ?>
     </div>
 <?php } ?>

 <?php if($this->session->flashdata('feedback_fail')) { ?>
     <div class="alert alert-danger">
         <?php echo $this->session->flashdata('feedback_fail');  ?>
     </div>
 <?php } ?>

<!-- Button trigger modal -->
<!-- <button type="button"  id="feedbackBtn" data-toggle="modal" data-target="#exampleModal">
  Feedback
</button>

<!-- Modal -->
<!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header"> -->
        <h3 class="modal-title" id="exampleModalLabel">Feedback</h3>
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="row justify-content-center" style="margin-top: 50px; background: white; overflow: auto;"> -->
    <!-- <div class="col-md-6 col-md-offset-3 form-container"> -->
       <div class="modal-body" style="background: ghostwhite"> 
           <p>
            Please provide your feedback below:
        </p>
        <form action="<?php echo base_url(); ?>View_Orders/feedback" method="post">
            <div class="row">
                <div class="col-sm-12 form-group">
                <label>How do you rate your overall experience at Doppio Zero?</label>
                <p>
                    <label class="radio-inline">
                    <input type="radio" name="experience" id="radio_experience" value="bad" >
                    Bad
                    </label>

                    <label class="radio-inline">
                    <input type="radio" name="experience" id="radio_experience" value="average" >
                    Average
                    </label>

                    <label class="radio-inline">
                    <input type="radio" name="experience" id="radio_experience" value="good" >
                    Good
                    </label>
                </p>
                </div>
            </div>  
            <div class="row">
                <div class="col-sm-12 form-group">
                    <label for="comments">
                        Comments:</label>
                    <textarea class="form-control" type="textarea" name="comments" id="comments" placeholder="Your Comments" maxlength="6000" rows="7"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-group">
                    <label for="name">
                        Your Name:</label>
                    <input type="text" class="form-control" id="name" name="name" required>
                </div>
                <div class="col-sm-6 form-group">
                    <label for="email">
                        Email:</label>
                    <input type="email" class="form-control" id="email" name="email" required>
                </div>
            </div>
            
                        <div class="row">
                <div class="col-sm-12 form-group">
                    <button type="submit" class="btn btn-lg btn-warning btn-block" >Post </button>
                </div>
            </div>

        </form>   
    </div>




