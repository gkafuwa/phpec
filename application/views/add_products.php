<?php if ($this->session->flashdata('prod_added')) { ?>
<div class="flash">
    <?php echo $this->session->flashdata('prod_added');  ?>
</div>
<?php } ?>
</div>

<form class="login" method="post" onsubmit="return addProd();" action="<?php echo base_url(); ?>products/add_product" enctype="multipart/form-data">
    <header>Add Product</header>
    <div class="valError" id="eRRname"></div>
    <input type="text" id="name" name="prodName" placeholder="Enter Product Name">
    <div class="valError" id="eRRpic"></div>
    <input type="file" id="pic" name="picture">
    <div class="valError" id="eRRprice"></div>
    <input type="text" id="price" name="price" placeholder="R 00.00">
    <div class="valError" id="eRRqty"></div>
    <input type="number" id="qty" name="qty" placeholder="Quantity">
    <div class="valError" id="eRRcatID"></div>
    <select id="catID" name="catID">
        <option value="" disabled selected hidden>Category</option>
        <option value="women">Women</option>
        <option value="neck">Neck</option>
        <option value="men">Men</option>
        <option value="oils">Oils</option>
        <option value="oils">Wrist</option>
    </select>
    <div class="valError" id="eRRisPerf"></div>
    <select id="isPerf" name="isPerfume">
        <option value="" disabled selected hidden>Type</option>
        <option value="1">Perfume</option>
        <option value="0">Jewerly</option>
    </select>
    <div class="valError" id="eRRisSpe"></div>
    <select id="isSpe" name="isSpecial">
        <option value="" disabled selected hidden>Specials or not</option>
        <option value="0">Not Special</option>
        <option value="1">Special</option>
    </select>
    <div class="valError" id="eRRdesc"></div>
    <textarea id="desc" name="description" placeholder="description"></textarea>
    <button type="submit" class="btn btn-primary"> Add Product</button>
</form>