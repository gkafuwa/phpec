<?php if ($this->session->flashdata('prod_updated')) { ?>
<div class="flash">
    <p class="head"> Info <span onclick="closeFlash();">&times</span> </p>
    <?php echo $this->session->flashdata('prod_updated');  ?>
</div>
<?php } ?>

<?php if ($this->session->flashdata('prod_deleted')) { ?>
<div class="flash flash-danger ">
    <p class="head"> Info <span onclick="closeFlash();">&times</span> </p>
    <p> <?php echo $this->session->flashdata('prod_deleted');  ?></p>
</div>
<?php } ?>
<header> Edit Products
</header>
<div id="add_search">
    <div id="search">
        <form action="<?php echo base_url(); ?>products/searchAdmin" method="POST">
            <input name="srch" type="text" placeholder="Search here...">
            <button type="submit"><i class="material-icons right">search</i></button>
        </form>
    </div>
</div>
<div class="infoSomething">Can sort the table by name or categories for easy retrieval</div>
<table id="tabThing">
    <tr>
        <!-- <th class="space">ID</th> -->
        <th class="space">Image</th>
        <th class="space" onclick="sortTable(1)" style="cursor: pointer">Name</th>
        <th class="space">Description</th>
        <th class="space">Price</th>
        <th class="space">QTY</th>
        <th class="space" onclick="sortTable(5)" style="cursor: pointer">Category</th>
        <th class="space">Special</th>
        <th class="space">Action</th>
    </tr>

    <?php foreach ($products as $prod) { ?>
    <tr>
        <form action="<?php echo base_url(); ?>products/update_prod" method="post">
            <!-- <td class="space">  -->
            <input type="hidden" name="id" style="width: 50%" value="<?php echo $prod->prodID ?>" />
            <!-- </td> -->
            <td class="space">
                <img src="<?php echo base_url(); ?>images/<?php echo $prod->image; ?>" alt="" width="100px" height="100px">
                <input hidden type="text" name="img" style="width: 90%" value="<?php echo $prod->image ?>" />
            </td>
            <td class="space"> <input type="text" name="name" style="width: 90%" value="<?php echo $prod->prodName ?>" /> </td>
            <td class="space"> <textarea rows="4" type="text" name="dscpt" style="width: 90%"><?php echo $prod->description ?> </textarea> </td>
            <td class="space"> <input type="text" name="price" style="width: 25%" value="<?php echo $prod->price ?>" /> </td>
            <td class="space"> <input type="text" name="qty" style="width: 25%" value="<?php echo $prod->qty ?>" /> </td>
            <td class="space"> <input type="text" name="catID" style="width: 40%" value="<?php echo $prod->categoryID ?>" /></td>
            <td class="space"> <input type="text" name="isSpecial" style="width: 40%" value="<?php echo $prod->isSpecial ?>" /></td>
            <td class="space">
                <button type="submit" style=" padding: 2px" class="fg-button teal">
                    <i class="material-icons">
                        edit
                    </i></button>
                <a class="delete" style=" padding: 2px; color: red" data-confirm="Are you sure to delete this item?" href="<?php echo base_url(); ?>products/deleteProd/<?php echo $prod->prodID ?>">
                    <i class="material-icons">
                        delete_forever
                    </i>
                </a>

            </td>
        </form>
    </tr>
    <?php } ?>
</table>

<script>
    var deleteLinks = document.querySelectorAll('.delete');

    for (var i = 0; i < deleteLinks.length; i++) {
        deleteLinks[i].addEventListener('click', function(event) {
            event.preventDefault();

            var choice = confirm(this.getAttribute('data-confirm'));

            if (choice) {
                window.location.href = this.getAttribute('href');
            }
        });
    }
</script>

<script>
    function sortTable(n) {
        var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
        table = document.getElementById("tabThing");
        switching = true;
        //Set the sorting direction to ascending:
        dir = "asc";
        /*Make a loop that will continue until
        no switching has been done:*/
        while (switching) {
            //start by saying: no switching is done:
            switching = false;
            rows = table.rows;
            /*Loop through all table rows (except the
            first, which contains table headers):*/
            for (i = 1; i < (rows.length - 1); i++) {
                //start by saying there should be no switching:
                shouldSwitch = false;
                /*Get the two elements you want to compare,
                one from current row and one from the next:*/
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                /*check if the two rows should switch place,
                based on the direction, asc or desc:*/
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                /*If a switch has been marked, make the switch
                and mark that a switch has been done:*/
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                //Each time a switch is done, increase this count by 1:
                switchcount++;
            } else {
                /*If no switching has been done AND the direction is "asc",
                set the direction to "desc" and run the while loop again.*/
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }
    }
</script>