<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="images/headerimg.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/css/index.css">
    <title>Home</title>
</head>

<body>
    <nav id="nav">
        <a class="index" href="#">
            <img src="images/headerimg.png" alt="">
            <h4>JFragle</h4>
        </a>
        <ul>
            <li> <a href="<?php echo base_url(); ?>users/customer_in"><i class="material-icons right">add_shopping_cart</i> </a></li>
            <li><a href="<?php echo base_url(); ?>products/about"><i class="material-icons right">info</i></a></li>
            <li><a href="<?php echo base_url(); ?>products/contactus"><i class="material-icons right">contact_mail</i></a></li>
        </ul>
    </nav>
    <div id="bg">
        <section id="primary">
            <h1>Welcome to JFragle</h1>
            <p>smell good, Fell good!</p>
            <a href="<?php echo base_url(); ?>users/customer_in">View all</a>
        </section>
    </div>
    <p class="title">Our lates products</p>
    <main>

        <div class="content" id="content">
            <?php foreach ($latest as $prod) {
                ?>
                <div class="cards">
                    <a href="<?php echo base_url(); ?>Products/details/<?php echo $prod->prodID; ?>">
                        <div class="info">
                            <div class="imagePlace">
                                <img src="images/<?php echo $prod->image; ?>">
                            </div>
                            <strong><?php echo $prod->prodName; ?></strong>
                            <hr class="pricething">
                            <p><?php echo $prod->description; ?></p>
                            <hr>
                        </div>
                    </a>
                </div>
            <?php }
        ?>
        </div>
        <div class="scro">

            <div id="scrollleft" onclick="scleft('content');">
                <i class="material-icons" onclick="scleft('content');">keyboard_arrow_left</i>
            </div>
            <div id="scrollright" onclick="scright('content');">
                <i class="material-icons" onclick="scright('content');">keyboard_arrow_right</i>
            </div>
        </div>

        <p> JFragle aims at making the best perfumes for anyone and everyone. Our Products contain mixtures of fragrant essential oils or aroma compounds, fixatives and solvents, used to give the human body, animals, food, objects, and living-spaces an agreeable scent. It is usually in liquid form and used to give a pleasant scent to a person's body.</p>

        <div id="foot">
        <a href="<?php echo base_url(); ?>users/login">Login</a>
            
            <!-- <div class="subscribe">
                <h4>Subscribe</h4>
                <form action="">
                    <input type="text" placeholder="Enter Name">
                    <input type="text" placeholder="Email address"> <br>
                    <button type="submit">Subscribe</button>
                </form>
                <span>Subscribe to our website and you'll recieve a 5% discount. You'll also recieve email pertaining to our new products</span>

            </div> -->

        </div>
    </main>
    <script src="assets/js/prods.js"></script>
</body>

</html>