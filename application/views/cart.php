<header>Cart</header>
<div class="contain">
    <!-- sub here -->
    <?php
    $this->load->view('subscribe');
    ?>

    <section id="cartPage">
        <?php if ($this->session->flashdata('item_changed')) { ?>
        <div class="flash flash-success">
            <section class="head"> Info <span onclick="closeFlash();">&times</span> </section>

            <?php echo $this->session->flashdata('item_changed');  ?>
        </div>
        <?php  } ?>

        <?php if ($this->session->flashdata('cartDone')) { ?>
        <div class="flash flash-success">
            <section class="head"> Info <span onclick="closeFlash();">&times</span> </section>

            <?php echo $this->session->flashdata('cartDone');  ?>
        </div>
        <?php  } ?>
        <?php if (!empty($this->cart->contents())) { ?>
        <table align="center" class="mainTab">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th>Total</th>
                </tr>
            </thead>

            <tr>
                <?php
                    $cart = $this->cart->contents(); ?>
                <?php foreach ($cart as $it) {
                        ?>
            <tr class="testTabOv">
                <form action="cart/update" method="post">
                    <input type="hidden" name="rowid" value="<?php echo $it['rowid']; ?>" />
                    <td> <img src="<?php echo base_url() . 'images/' . $it['image']; ?>"></td>
                    <td> <a href="<?php echo base_url(); ?>Products/details/<?php echo $it['id']; ?>"> <?php echo $it['name']; ?></a></td>
                    <td class="qty">
                        <input name="qty" type="number" value="<?php echo $it['qty']; ?>">
                        <button type="submit" class="ren"><i class="material-icons">autorenew</i></button>
                        <?php
                                $path = '<i class="material-icons del">remove_shopping_cart</i>';
                                echo anchor('cart/delete/' . $it['rowid'], $path); ?>
                    </td>
                    <td> R <?php echo number_format($it['price'], 2); ?> </td>
                    <td>R <?php echo $it['subtotal']; ?></td>
                </form>
            </tr>
            <?php } ?>
        </table>
        <p>What would you like to do next?</p>
        <table align="center" class="total">
            <tbody>
                <tr>
                    <td class="leftis">Sub Total:</td>
                    <td><?php echo $this->cart->format_number($this->cart->total()); ?></td>
                </tr>
                <?php $vat =  $this->cart->format_number($this->cart->total()) * 0.15; ?>
                <tr>
                    <td class="leftis">VAT(15%):</td>
                    <td><?php echo $vat; ?></td>
                </tr>
                <tr>
                    <td class="leftis"> <strong> Grand Total:</strong></td>
                    <td> <strong id="totalPay"> <?php echo $this->cart->format_number($this->cart->total()) + $vat; ?></strong></td>
                </tr>
                <tr>
                    <td><a href="<?php echo base_url(); ?>products" class="cont"><i class="material-icons right">arrow_back</i>Continue Shoping</a></td>
                    <td><a onclick="checkout()" class="checkout">Check out <i class="material-icons right">arrow_forward</i></a></td>
                </tr>
            </tbody>
        </table>
        <form id="checkOutForm" action="<?php echo base_url(); ?>cart/print_cart" onsubmit="return checkoutValidate();" method="post">
            <span class="heading">Personal details</span>
            <div class="valError" id="eRRfName"></div>
            <input type="text" placeholder="First Name" name="fName" id="fName">
            <div class="valError" id="eRRlName"></div>
            <input type="text" placeholder="Last Name" name="lName" id="lName">
            <div class="valError" id="eRRemail"></div>
            <input type="text" placeholder="Email" name="email" id="email">
            <div class="valError" id="eRRtel"></div>
            <input type="text" placeholder="Telephone" name="tel" id="tel">
            <div class="valError" id="eRRaddress"></div>
            <input type="text" placeholder="Address" name="address" id="address">
            <div class="valError" id="eRRzip"></div>
            <input type="text" placeholder="Zipcode" name="zip" id="zip">
            <span class="heading">Payment Method</span>
            <div class="valError" id="eRRpty"></div>
            <select name="pty" id="pyt">
                <option value="" disabled selected hidden>Payment Methode</option>
                <option value="COD">Cash on delivery</option>
                <option value="pp">Pay Pal</option>
            </select>
            <div id="pay"></div>
            <span class="heading">Delivery Options</span>
            <div class="valError" id="eRRdel"></div>
            <select name="delivery" id="del">
                <option value="" disabled selected hidden>Delivery Type</option>
                <option value="Del">Deliver to Adrress</option>
                <option value="sc">Self Collection</option>
            </select>
            <button type="submit">Proceed Checkout</button>
        </form>
    </section>
    <script src="https://www.paypal.com/sdk/js?client-id=sb"></script>
    <script src="<?php echo base_url(); ?>assets/js/event.js"></script>

    <!-- <script src="https://www.paypal.com/sdk/js?client-id=sb&currency=ZAR"></script> -->

    <?php } else { ?>
    <p> Your cart is empty</p>
    <a href="<?php echo base_url(); ?>products" class="cont">Add Products</a>
</div>
<?php } ?>