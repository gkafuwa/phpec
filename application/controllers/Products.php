<?php
//classes should start with a CAP
class Products extends CI_Controller
{

    public function index()
    {

        if (!empty($this->session->userdata('user_id'))) {

            //     //Get all products 
            $data['products'] = $this->Products_model->get_products();

            //Loading view.
            $data['main_content'] = 'products';
            $this->load->view('layouts/main', $data);
        } else {

            $data['latest'] = $this->Products_model->get_latest_prod();
            $this->load->view('home', $data);
        }
    }

    public function details($id)
    {
        //get the product details from the model
        $data['prod'] = $this->Products_model->get_product_details($id);
        $data['ralated'] = $this->Products_model->get_product_related($data['prod']->categoryID);
        //Load view
        $data['main_content'] = 'details';
        $this->load->view('layouts/main', $data);
    }

    public function category($id)
    {
        //get the product details from the model
        $data['products'] = $this->Products_model->get_categories_view($id);
        //Load view
        $data['main_content'] = 'categories';
        $this->load->view('layouts/main', $data);
    }
    public function contactus()
    {
        //Load view
        $data['main_content'] = 'contact';
        $this->load->view('layouts/main', $data);
    }
    public function about()
    {
        //Load view
        $data['main_content'] = 'about';
        $this->load->view('layouts/main', $data);
    }

    public function add()
    {
        //Load view
        $data['main_content'] = 'add_products';
        $this->load->view('layouts/admin', $data);
    }
    public function edit()
    {
        $data['products'] = $this->Products_model->get_products();

        //Load view
        $data['main_content'] = 'product_editor';
        $this->load->view('layouts/admin', $data);
    }



    public function add_product()
    {

        $prodName = $this->input->post('prodName');
        $description = $this->input->post('description');
        $price = $this->input->post('price');
        $categoryID = $this->input->post('catID');
        $isPerfum = $this->input->post('isPerfume');
        $qty = $this->input->post('qty');
        $isNew = date("Y-m-d");
        $isSpecial = $this->input->post('isSpecial');


        //The parts that were changed were the, image value in the array section
        if (!empty($_FILES['picture']['name'])) {
            $config['upload_path'] = 'images';
            $config['allowed_types'] = 'jpg|jpeg|png|gif|bmp';
            $config['file_name'] = $_FILES['picture']['name'];

            //load the libraries
            // $this->load->library('upload', $config);
            $this->upload->initialize($config);

            //The statement is returning false meanig 
            if ($this->upload->do_upload('picture')) {
                $uploadData = $this->upload->data();
                $picture = $uploadData['file_name'];
            } else {
                $picture = 'notfound.png';
            }
        } else {
            $picture = 'something happened';
        }


        $data = array(
            // 'prodID' => $this->input->post('prodID'),
            'prodName' => $prodName,
            'image' => $picture,
            'description' => $description,
            'price' => $price,
            'categoryID' => $categoryID,
            'isPerfume' => $isPerfum,
            'qty' => $qty,
            'isNew' => $isNew,
            'isSpecial' => $isSpecial
        );

        if ($this->Products_model->add_prod($data)) {
            $this->session->set_flashdata('prod_added', 'The Product has been added to the database');
            redirect('products/add');
        } else {
            echo 'something went wrong, so go and recheck the code and make sure trhat egverything is done accordingly';
        }
    }

    public function search()
    {
        $srch = $this->input->post('srch');
        $data['products'] = $this->Products_model->search_results($srch);
        //Loading view.
        if (!empty($data['products'])) {
            $this->session->set_flashdata('search', 'If you are searching for jewerly, click under the jewerly tab to view them');
        } else {
            $this->session->set_flashdata('search_invalid', 'The Product you searched for does not exist in our inventory
           Please make sure that you entered the correct values !!');
        }

        $data['main_content'] = 'products';
        $this->load->view('layouts/main', $data);
    }

    public function searchAdmin()
    {
        $srch = $this->input->post('srch');
        $data['products'] = $this->Products_model->search_results($srch);
        //Loading view.
        if (!empty($data['products'])) {
            $this->session->set_flashdata('search', 'If you are searching for jewerly, click under the jewerly tab to view them');
        } else {
            $this->session->set_flashdata('search_invalid', 'The Product you searched for does not exist in our inventory
           Please make sure that you entered the correct values !!');
        }

        $data['main_content'] = 'product_editor';

        $this->load->view('layouts/admin', $data);
    }

    public function view_all()
    {
        if (!empty($this->session->userdata('user_id'))) {
            // $this->load->view('layouts/edit');
            redirect('products/edit');
        } else {
            redirect('products/edit');
        }
    }

    public function update_prod()
    {
        $prodName = $this->input->post('name');
        $data = array(
            'prodID' => $this->input->post('id'),
            'prodName' => $this->input->post('name'),
            'image' =>  $this->input->post('img'),
            'description' => $this->input->post('dscpt'),
            'price' => $this->input->post('price'),
            'qty' => $this->input->post('qty'),
            'categoryID' => $this->input->post('catID'),
            'isSpecial' => $this->input->post('isSpecial')
        );

        $id = $this->input->post('id');

        if ($this->Products_model->update_prod_table($id, $data)) {
            $this->session->set_flashdata('prod_updated', $prodName.' has been successfully updated');
            $this->view_all();
        } else {
            echo 'Something went wrong';
        }
    }

    public function deleteProd($del)
    {
        if ($this->Products_model->delete_prod($del)) {
            $this->session->set_flashdata('prod_deleted', 'The Product has been successfully deleted');
            $this->view_all();
        } else {
            echo 'Something went wrong';
        }
    }
}
