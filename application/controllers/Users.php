<?php
class Users extends CI_Controller
{

    public function register()
    {
        if ($this->User_model->register()) {
            $this->session->set_flashdata('registered', 'new user has been added');
            redirect('users/register');
        }
    }

    //tis will view the login form/view
    public function login()
    {
        $data['main_content'] = 'login';
        $this->load->view('layouts/admin', $data);
    }
    public function menu()
    {
        $data['chart'] = $this->Products_model->get_chart();
        $data['chartCat'] = $this->Products_model->get_chartCat();
        $data['all'] = $this->Products_model->get_all();
        $data['pending'] = $this->Products_model->get_pending();

        $data['main_content'] = 'menu';
        $this->load->view('layouts/admin', $data);
    }

    // this will authenticate the user login details 
    // and redirect them to the products page
    public function logins()
    {
        $username = $this->input->post('username'); //this is the same as saying $_POST['userID_l']
        $pass = $this->input->post('pass');


        $user_id = $this->User_model->logins($username, $pass);

        print_r($user_id);
        // validate user
        if ($user_id) {
            //create an array of user data
            $data = array(
                'username' => $username,
                'logged_in' => true
            );

            //set session userdata
            $this->session->set_userdata($data);

            $this->session->set_flashdata('pass_login', 'You are now logged in');
            redirect('users/menu');
        } else {
            $this->session->set_flashdata('fail_login', 'Sorry login information is incorrect');
            redirect('users/login');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('username');
        $this->session->sess_destroy();

        redirect('products');
    }

    public function subscribe()
    {
        //Under the string $Caracteres you write all the characters you want to be used to randomly generate the code.  
        $Caracteres = 'A*B?C/D$EF#GHIJKLMOPQRSTUVXWYZ0123456789abcdefghijklmnopqrstuvwxyz';
        $QuantidadeCaracteres = strlen($Caracteres);
        $QuantidadeCaracteres--;

        $Hash = NULL;
        for ($x = 1; $x <= 10; $x++) {
            $Posicao = rand(0, $QuantidadeCaracteres);
            $Hash .= substr($Caracteres, $Posicao, 1);
        }




        redirect('cart');
    }

    //this will create a session for the customers so there is no conflict when they make an order
    public function customer_in()
    {
        $data = array(
            'user_id' => "Hello",
            'timestamp' => date('y-m-d H:i:s')
        );

        $this->session->set_userdata($data);
        $this->session->set_flashdata('customer_login', 'You can add to cart the products of your choice. if you would like more than one of the same product you can do so by increasing the QTY value');
        redirect('products');
    }
}
