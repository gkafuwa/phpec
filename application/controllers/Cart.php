<?php
class Cart extends CI_Controller
{

    public $paypal_data = '';
    public $tax;
    public $total = 0;
    public $grand_total;

    //Cart index funcion    
    public function index()
    {
        $data['main_content'] = 'cart';
        $this->load->view('layouts/main', $data);
    }


    //this will add values to the cart when the 'add to cart' button is clicked
    public function add()
    {
        // Set array for cart data.        
        $prodID = $this->input->post('prodID');
        $prodName = $this->input->post('prodName');
        $price = $this->input->post('price');
        $qty = $this->input->post('qty');
        $image = $this->input->post('image');
        $catID = $this->input->post('catID');

        $insert_data = array(
            'image' => $image,
            'id' => $prodID,
            'name' => $prodName,
            'price' => $price,
            'qty' => $qty,
            'catID' => $catID,

        );

        // This function add items into cart        
        $this->cart->insert($insert_data);
        $this->session->set_flashdata('addded', $qty . ' ' . $prodName . ' has been adde to your cart');

        // This will show insert data in cart.
        redirect('products');
    }


    //This function will update the cart according to the values entered by the user
    public function update()
    {

        $qty = $this->input->post('qty');
        $id = $this->input->post('rowid');

        $data = array(
            'rowid' => $id,
            'qty' => $qty
        );

        $this->cart->update($data);
        $this->session->set_flashdata('item_changed',  'Item quantity updated');

        redirect('cart');
    }

    //this will clear everything from the cart
    public function clear_cart()
    {
        $this->cart->destroy();
        redirect('cart');
    }

    //This function will place the order from the cart into the database
    public function print_cart()
    {
        $name = $this->input->post('fName') . "_" . $this->input->post('lName');
        $email = $this->input->post('email');
        $telephone = $this->input->post('tel');
        $address = $this->input->post('address');
        $zip = $this->input->post('zip');
        $delivery = $this->input->post('delivery');
        $pty = $this->input->post('pty');

        $data = array(
            'telephone' => $telephone,
            'name' => $name,
            'email' => $email,
            'address' => $address,
            'zip' => $zip,

            'delivery' => $delivery,
        );

        $id = array(
            'ids' => $name,
            'pty' => $pty,
            // 'deliveryAddress' => $deliveryAddress,
        );
        $cartContentString = serialize($this->cart->contents());

        $cartArray = unserialize($cartContentString);

        foreach ($cartArray as $it) {
            $this->Orders_model->print_cart(array_merge($id, $it));
        }
        $this->Orders_model->add_customer($data, $name);
        $this->session->set_flashdata('cartDone', "Your order has been successfully placed. Thank you!");
        $this->clear_cart();
    }




    //This function will delete an order from the cart
    public function delete($id)
    {

        $data = array(
            'rowid' => $id,
            'qty' => 0
        );

        $this->session->set_flashdata('item_changed',  'Item has been removed from cart');
        $this->cart->update($data);
        redirect('cart', 'refresh');
    }

    //this function will allow the waiter to print the bill 
    //and change the status of the order to completed(1) and print out the values as a bill
    public function check_out()
    {
        $ids = $this->input->post('ids');

        // will create an update array so that the table active column changes to 0
        $data = array(
            'status' => 1
        );

        $this->Orders_model->updateTable($ids, $data);
        $this->Orders_model->change_status($ids);
        $this->session->set_flashdata('order_done',  'customers orders have been completed');
        redirect('view_orders');

    }
}
