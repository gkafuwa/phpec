<?php
/* This controller will initiate the Orders view and display them to the waiter/kitchen */

class View_Orders extends CI_Controller
{

    public function index()
    {
        $data['orders'] = $this->Orders_model->get_users();
        $data['main_content'] = 'view_orders';
        $this->load->view('layouts/admin', $data);
    }

    public function for_one($name)
    {
        $data['orders'] = $this->Orders_model->view($name);

        $data['main_content'] = 'for_one';
        $this->load->view('layouts/admin', $data);
    }

    //this is a test function until otherwise
    public function for_tab($tNum)
    {

        $data['orders'] = $this->Orders_model->view($tNum);

        $data['main_content'] = 'view_orders';
        $this->load->view('layouts/testTab', $data);
    }


    public function feedback()
    {

        $data = array(
            'comment' => $this->input->post('message'),
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone'),

        );



        if ($this->Orders_model->feedback($data)) {
            $this->session->set_flashdata('feedback', 'Thank you for your feedback which will be used to improve our services');
            redirect('products/contactus');
        } else {
            $this->session->set_flashdata('feedback_fail', 'There seems to be something wrong with the data that you entered');
            redirect('products/contactus');
        }
    }

    public function viewFeedback()
    {
        if (!empty($this->session->userdata('logged_in'))) {
            $data['feedback'] = $this->Orders_model->viewFeedback();

            $data['main_content'] = 'viewFeedback';
            $this->load->view('layouts/admin', $data);
        } else {
            redirect('products');
        }
    }


    public  function sendMail()
    {

        $message =
            " This is the message from the site";

        $this->load->library('email');
        $this->email->set_newline("\r\n");
        $this->email->from('be2015-1303@educationgroup.co.za'); // change it to yours
        $this->email->to('gkafuwa@gmail.com'); // change it to yours
        $this->email->subject('Testing Codeigniter');
        $this->email->message($message);
        if ($this->email->send()) {
            echo 'Email sent.';
        } else {
            show_error($this->email->print_debugger());
        }
    }
}
